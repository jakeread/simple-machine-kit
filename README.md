# Simple Machine Kit

> Circa [Machine Week 2023](https://gitlab.cba.mit.edu/jakeread/machineweek-2023)

This is a simple set of machine axes that you can print and assemble using parts from [this kit](KIT.md). 

### Belt Axis [[CAD](CAD/belt_axis/belt_axis.f3z)]

<img src=img/belt_axis.png width=50%><br>

Kit [[stl](CAD/belt_axis/belt_axis_kit.stl)]

<img src=img/belt_axis_kit.png width=50%><br>

### Leadscrew Axis

Assembly [[CAD](CAD/leadscrew_axis/leadscrew_axis.f3z)]

<img src=img/leadscrew_axis.png width=50%><br>

Kit [[stl](CAD/leadscrew_axis/leadscrew_axis_kit.stl)]

<img src=img/leadscrew_axis_kit.png width=50%><br>

### Rotary Axis

Assembly [[CAD](CAD/rotary_axis/rotary_axis.f3z)] [[notes](https://ekswhyzee.com/2019/04/09/gt2-belt-rotary-cad.html)]

<img src=img/rotary_axis.png width=50%><br>

Kit [[stl](CAD/rotary_axis/rotary_axis_kit.stl)]

<img src=img/rotary_axis_kit.png width=50%><br>

### Framing

T gusset [[CAD](CAD/parts/gusset_T.f3z)][[stl](CAD/parts/gusset_T.stl)]

<img src=img/gusset_T.png width=50%><br>

Elbow gusset [[CAD](CAD/parts/gusset_elbow.f3z)][[stl](CAD/parts/gusset_elbow.stl)]

<img src=img/gusset_elbow.png width=50%><br>
